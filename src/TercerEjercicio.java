import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TercerEjercicio {
	/* En este ejercicio tenemos que pedir valores necesarios 
	 * para dibujar un triangulo, para mi necesitamos saber los 3 lados
	 */
	float base, altura, hipotenusa; // Declaro los lados
	public void resolver() {
		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("-----------------------------------------------------"); //Linea separadora
			System.out.println("Calcularemos el area y perimetro de un triangulo");
			System.out.println("Debera ingresar los 3 lados de un triangulo");
			
			//Ingreso los lados
			System.out.println("Ingrese BASE.");
			base = Float.parseFloat(entrada.readLine());
			
			System.out.println("Ingrese ALTURA.");
			altura = Float.parseFloat(entrada.readLine());
			
			System.out.println("Ingrese la HIPOTENUSA.");
			hipotenusa = Float.parseFloat(entrada.readLine());

			float area = (base*altura)/2;
			float perimetro = base+altura+hipotenusa;
			
			
			//Muestro el area y perimetro
			System.out.println("Perimetro: "+perimetro+" Area: "+area);
			
		}catch(Exception exc) {
			System.out.println("Ocurrio  un error."+exc);
		}
	}
}
