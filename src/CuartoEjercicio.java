import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CuartoEjercicio {
	String nombre, ocupacion;
	Byte edad;
	Float altura;
	public void resolver() {
		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("-----------------------------------------------------"); //Linea separadora
			System.out.println("Ingrese su nombre: ");
			nombre = entrada.readLine();
			System.out.println("Ingrese su edad: ");
			edad = (byte) Integer.parseInt(entrada.readLine()); //Convierto a INT y de INT a Byte
			System.out.println("Ingrese su altura (EN NUMEROS, SIN INDICAR CM O METROS): ");
			altura = Float.parseFloat(entrada.readLine());
			
			System.out.println("Nombre: "+nombre);
			System.out.println("Edad: "+edad);
			System.out.println("Altura: "+altura);

		}catch(Exception exc) {
			System.out.println("Ocurrio  un error."+exc);
		}
	}
}
