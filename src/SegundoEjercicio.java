import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SegundoEjercicio {
	final float pi = (float)3.14159265359; //Declaro e inicializo la constante PI
	float radio, circunferencia, area;
	public void resolver() {
		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("----------------------------------------------------------------"); //Linea separadora
			System.out.println("Ahora calcularemos la circunferencia de un circulo"); //Indicamos al usuario que vamos a hacer
			System.out.println("Ingrese el radio");
			
			radio = Float.parseFloat(entrada.readLine());
			
			// Ya que 2 * pi * radio = circunferencia
			circunferencia = 2 * pi * radio;
			
			System.out.println("Circunferencia: "+circunferencia);

		}catch(Exception exc) {
			System.out.println("Ocurrio  un error."+exc);

		}
	}
}
