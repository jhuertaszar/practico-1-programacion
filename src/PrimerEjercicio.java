import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PrimerEjercicio {
	int primerNumero, segundoNumero, tercerNumero, cuartoNumero, quintoNumero;
	public void resolver() {
		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
		try {
			
			//Guardo los 5 números ENTEROS en las variables declaradas al principio de la clase
			System.out.println("Mostraremos 5 numeros ENTEROS en orden inverso al ingresado"); 
			System.out.println("Ingrese el primer numero entero.");
			primerNumero = Integer.parseInt(entrada.readLine());
			System.out.println("Ingrese el segundo numero entero.");
			segundoNumero = Integer.parseInt(entrada.readLine());
			System.out.println("Ingrese el tercer numero  entero.");
			tercerNumero = Integer.parseInt(entrada.readLine());
			System.out.println("Ingrese el cuarto numero  entero.");
			cuartoNumero = Integer.parseInt(entrada.readLine());
			System.out.println("Ingrese el quinto numero  entero.");
			quintoNumero = Integer.parseInt(entrada.readLine());
			
			//Informo al usuario que es lo que voy a mostrar y lo muestro
			System.out.println("Se muestran los numeros en orden inverso.");
			System.out.println(quintoNumero+" "+cuartoNumero+" "+tercerNumero+" "+segundoNumero+" "+primerNumero);
			
			
		} catch (Exception e) {
			System.out.println("Ocurrio  un error."+e);
		} 
	}
}
