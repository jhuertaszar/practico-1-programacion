import java.io.BufferedReader;
import java.io.InputStreamReader;

public class QuintoEjercicio {
	int numeroDeFactura;
	String tipoDeFactura,nombreDelCliente;
	int importeProducto1, importeProducto2;
	String nombreProducto1, nombreProducto2;
	
	public void resolver() {
		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("-----------------------------------------------------"); //Linea separadora
			
			System.out.println("Ingrese nombre del cliente");
			nombreDelCliente = entrada.readLine();
			
			System.out.println("Ingrese tipo de factura");
			tipoDeFactura = entrada.readLine();
			
			System.out.println("Ingrese el número de factura");
			numeroDeFactura = Integer.parseInt(entrada.readLine());
			
			System.out.println("Ingrese nombre del primer producto");
			nombreProducto1 = entrada.readLine();
			System.out.println("Ingrese el importe del primer producto");
			importeProducto1 = Integer.parseInt(entrada.readLine());
			
			System.out.println("Ingrese nombre del segundo producto");
			nombreProducto2 = entrada.readLine();
			System.out.println("Ingrese el importe del primer producto");
			importeProducto2 = Integer.parseInt(entrada.readLine());
			
			float total = importeProducto1 + importeProducto2; //Sumo los dos productos y calculo el total
			
			
			//Mostramos los datos de la factura
			System.out.println(); //Dejo un espacio
			
			System.out.println("Factura		"+tipoDeFactura+"	N "+numeroDeFactura);
			System.out.println(nombreDelCliente);
			System.out.println("Producto	Importe");
			System.out.println(nombreProducto1+"	"+importeProducto1);
			System.out.println(nombreProducto2+"	"+importeProducto2);
			System.out.println("Total		"+total);

			
		}catch(Exception exc) {
			System.out.println("Ocurrio  un error."+exc);

		}
	}
	
}
